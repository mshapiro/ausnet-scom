# Class: scom
#
#  Manages the scom package and service
#
# Parameters:
#
#  [enabled]
#   Desc: Determine whether the scom service should be installed and running
#   Args: <boolean>
#   Default: true
#
# Sample Usage:
#
#  class { 'scom': enabled => true, }
#
class scom(
  $enabled = false,
  $package_name = 'scx',
  $service_name = 'scx-cimd')
  {
  case $::osfamily {
    'RedHat': {
    }
    default: {
      fail("The ${module_name} module is untested on ${::osfamily} based systems.")
    }
  }
  if $enabled {
    $package_ensure = 'present'
    $service_ensure = 'running'
    $service_enable = true
    Package["$package_name"] -> Service["$service_name"]
  }
  else {
    $package_ensure = 'absent'
    $service_ensure = 'stopped'
    $service_enable = false
    Service["$service_name"] -> Package["$package_name"]
  }
  # resources
  package { "$package_name":
    ensure  =>  $package_ensure,
  }
  service { "$service_name":
    ensure      => $service_ensure,
    enable      => $service_enable,
    hasrestart  => true,
    hasstatus   => true,
  }
}

